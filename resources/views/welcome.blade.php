<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>Miu beauty light - Spa - Nail</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css">
  <link rel="stylesheet" href="assets/css/Advanced-NavBar---Multi-dropdown.css">
  <link rel="stylesheet" href="assets/css/best-carousel-slide.css">
  <link rel="stylesheet" href="assets/css/Block-Responsive-Item-List.css">
  <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
  <link rel="stylesheet" href="assets/css/Footer-Basic.css">
  <link rel="stylesheet" href="assets/css/Footer-Dark.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
  <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
  <!-- Start: Advanced NavBar - Multi-dropdown -->
  <div class="header2 bg-success-gradiant">
    <div class="">
      <!-- Header 1 code -->
      <nav class="navbar navbar-expand-lg h2-nav">
        <a class="navbar-brand" href="#"><img
            src="https://www.wrappixel.com/demos/ui-kit/wrapkit/assets/images/logos/white-logo.png" alt="wrapkit" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header2"
          aria-controls="header2" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-menu"></span>
        </button>
        <div class="collapse navbar-collapse hover-dropdown" id="header2">
          <ul class="navbar-nav">
            <li class="nav-item active"><a class="nav-link" href="#">Trang chủ</a></li>
            <li class="nav-item dropdown position-relative">
              <a class="nav-link dropdown-toggle" href="#" id="h2-dropdown" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                Dịch vụ <i class="icon-arrow-down ml-1 font-12"></i>
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Action</a></li>
              </ul>
            </li>
            <li class="nav-item"><a class="nav-link" href="#">Liên hệ</a></li>
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="btn rounded-pill btn-dark py-2 px-4" target="_blank" href="/register">Đăng ký thành viên</a></li>
          </ul>
        </div>
      </nav>
      <!-- End Header 1 code -->
    </div>
  </div>
  <!-- End: Advanced NavBar - Multi-dropdown -->
  <!-- Start: Contact Form Clean -->
  <div class="contact-clean" style="padding: 0px;">
    <!-- Start: best carousel slide -->
    <section id="carousel">
      <!-- Start: Carousel Hero -->
      <div class="carousel slide" data-ride="carousel" id="carousel-1">
        <div class="carousel-inner">
          <div class="carousel-item">
            <div class="jumbotron pulse animated hero-nature carousel-hero">
              <h1 class="hero-title">Hero Nature</h1>
              <p class="hero-subtitle">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus
                ac facilisis in, egestas eget quam.</p>
              <p><a class="btn btn-primary hero-button plat" role="button" href="#">Learn more</a></p>
            </div>
          </div>
          <div class="carousel-item">
            <div class="jumbotron pulse animated hero-photography carousel-hero">
              <h1 class="hero-title">Hero Photography</h1>
              <p class="hero-subtitle">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus
                ac facilisis in, egestas eget quam.</p>
              <p><a class="btn btn-primary hero-button plat" role="button" href="#">Learn more</a></p>
            </div>
          </div>
          <div class="carousel-item active">
            <div class="jumbotron pulse animated hero-technology carousel-hero" style="padding: 100px 32px;">
              <h1 class="hero-title">Miu beauty light</h1>
              <p class="hero-subtitle">Spa - Nail</p>
              <p><a class="btn btn-primary hero-button plat" role="button" href="#">Về chúng tôi</a></p>
            </div>
          </div>
        </div>
        <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><i
              class="fa fa-chevron-left"></i><span class="sr-only">Previous</span></a><a class="carousel-control-next"
            href="#carousel-1" role="button" data-slide="next"><i class="fa fa-chevron-right"></i><span
              class="sr-only">Next</span></a></div>
        <ol class="carousel-indicators">
          <li data-target="#carousel-1" data-slide-to="0"></li>
          <li data-target="#carousel-1" data-slide-to="1"></li>
          <li data-target="#carousel-1" data-slide-to="2" class="active"></li>
        </ol>
      </div>
      <!-- End: Carousel Hero -->
    </section>
    <!-- End: best carousel slide -->
  </div>
  <!-- End: Contact Form Clean -->
  <!-- Start: Registration Form with Photo -->
  <div class="register-photo">
    <!-- Start: Form Container -->
    <div class="form-container">
      <!-- Start: Image -->
      <div class="image-holder"></div>
      <!-- End: Image -->
      <form>
        <h2 class="text-center" style="font-size: 19px;"><strong>Đặt lịch để không phải chờ đợi</strong></h2>
        <div class="form-group"><input required class="form-control" type="text" placeholder="Nhập tên Anh / Chị"></div>
        <div class="form-group"><input required class="form-control" type="tel" placeholder="Nhập số điện thoại"></div>
        <div class="form-group">
          <select class="form-control" required>
            <option selected disabled value="">Chọn giờ</option>
            <option value="9">9h</option>
            <option value="10">10h</option>
            <option value="11">11h</option>
            <option value="12">12h</option>
            <option value="13">13h</option>
            <option value="14">14h</option>
            <option value="15">15h</option>
            <option value="16">16h</option>
            <option value="17">17h</option>
            <option value="18">18h</option>
            <option value="19">19h</option>
            <option value="20">20h</option>
            <option value="21">21h</option>
            <option value="22">22h</option>
          </select>
        </div>
        <select class="form-control" required>
          <option selected disabled value="">Chọn dịch vụ</option>
          <option value="1">Chăm sóc da mặt cơ bản</option>
          <option value="2">Chăm sóc da mặt nâng cao</option>
          <option value="3">Làm móng cơ bản</option>
          <option value="4">Làm móng nâng cao</option>
          <option value="5">Gội đầu cơ bản</option>
          <option value="6">Gội đầu dầu cặp</option>
        </select>
        <div class="form-group"><button type="submit" class="btn btn-primary btn-block" type="submit">Đặt chỗ
            ngay</button></div>
      </form>
    </div>
    <!-- End: Form Container -->
  </div>
  <!-- End: Registration Form with Photo -->
  <!-- Start: Footer Dark -->
  <div class="footer-dark">
    <footer>
      <div class="container">
        <!-- Start: Copyright -->
        <a href="tel:+84968667361"><h3 class="text-center">Liên Hệ: 0968 667 361</h3></a>
        <h3 class="text-center">Miu beauty light © 2021</h3>
        <!-- End: Copyright -->
      </div>
    </footer>
  </div>
  <!-- End: Footer Dark -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/Advanced-NavBar---Multi-dropdown.js"></script>
</body>

</html>